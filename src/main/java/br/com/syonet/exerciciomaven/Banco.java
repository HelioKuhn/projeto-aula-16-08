package br.com.syonet.exerciciomaven;

import java.util.Objects;

public class Banco {

	private String nome;
	private String código;

	public Banco(String nome, String código) {
		super();
		this.nome = nome;
		this.código = código;
	}

	@Override
	public String toString() {
		return "Banco [nome=" + nome + ", código=" + código + "]";
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Banco other = (Banco) obj;
		return Objects.equals(código, other.código) && Objects.equals(nome, other.nome);
	}
	
	
	
}
