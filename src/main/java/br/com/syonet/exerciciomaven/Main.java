package br.com.syonet.exerciciomaven;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import br.com.syonet.exerciciomaven.exceptions.ContaNaoEncontradaException;
import br.com.syonet.exerciciomaven.exceptions.SaldoInsuficienteException;

public class Main {

	public static List<Conta> contas;

	public static void main(String[] args) {

		Banco bancoBradesco = new Banco("BRADESCO", "1212");
		Banco bancoItau = new Banco("ITAU", "1313");
		Banco bancoCaixa = new Banco("CAIXA", "1414");

		Titular titularConta = new Titular("Hélio", "02492417069");

		Titular titularConta2 = new Titular("Hélio", "02492417068");

		Titular titularConta3 = new Titular("Hélio", "02492417067");

		contas = Arrays.asList(
				new Conta(bancoBradesco, "1491", "24443-4", 1239.60, EnumTipoConta.CORRENTE, titularConta),
				new Conta(bancoItau, "1636", "24443-4", 2355.60, EnumTipoConta.SALARIO, titularConta2),
				new Conta(bancoCaixa, "1491", "95635-4", 45466.60, EnumTipoConta.POUPANCA, titularConta3),
				new Conta(bancoBradesco, "1465", "4454-4", 1239.60, EnumTipoConta.POUPANCA, titularConta));
		
		
		
		try {
			List<Conta> buscarContasCpf = buscarContasPorCpf("02492417069");
			System.out.println("Listar Contas encontradas:");
			buscarContasCpf.forEach(System.out::println);
			System.out.println("Retornando erro de contas não encontrada:");
			buscarContasCpf = buscarContasPorCpf("02492417065");
		} catch (ContaNaoEncontradaException contaNaoEncontradaException) {
			contaNaoEncontradaException.printStackTrace();
		}
		
		try {
			Conta buscarContaPorDadosConta = consultarConta(bancoBradesco, "1491", "24443-4");
			System.out.println("Retornando conta encontrada:");
			System.out.println(buscarContaPorDadosConta);
			System.out.println("Retornando erro de conta não encontrada:");
			buscarContaPorDadosConta = consultarConta(bancoBradesco, "1495", "24443-4");
		} catch (ContaNaoEncontradaException contaNaoEncontradaException) {
			contaNaoEncontradaException.printStackTrace();
		}
		
		
		try {
			Conta buscarContaEDiscontarSaldo = Saque(bancoBradesco, "1491", "24443-4", 500.00);
			System.out.println("Listar Contas encontrada e retornado a conta com o valor atual do saldo:");
			System.out.println(buscarContaEDiscontarSaldo);
			System.out.println("Retornando erro de saldo insuficiente:");
			buscarContaEDiscontarSaldo = Saque(bancoBradesco, "1491", "24443-4", 34444.00);
		} catch (ContaNaoEncontradaException contaNaoEncontradaException) {
			contaNaoEncontradaException.printStackTrace();
		} catch (SaldoInsuficienteException saldoInsuficienteException) {
			saldoInsuficienteException.printStackTrace();
		}

	}

	public static List<Conta> buscarContasPorCpf(String cpf) throws ContaNaoEncontradaException {
		List<Conta> contasEncontradas =  contas.stream()
		.filter(c -> c.getTitular().getCpf().equals(cpf))
		.collect(Collectors.toList());
		if(contasEncontradas.size() == 0) {
			throw new ContaNaoEncontradaException();
		}
		return contasEncontradas;
	}
	
	public static Conta consultarConta(Banco banco, String agencia, String numeroConta ) throws ContaNaoEncontradaException {
		return contas.stream()
				.filter(c -> c.getBanco().equals(banco))
				.filter(c -> c.getAgencia().equals(agencia))
				.filter(c -> c.getNumeroConta().equals(numeroConta))
				.findAny()
				.orElseThrow(() -> new ContaNaoEncontradaException());
	}
	
	public static Conta Saque(Banco banco, String agencia, String numeroConta, Double valorSaque ) throws ContaNaoEncontradaException, SaldoInsuficienteException {
		Conta conta = consultarConta(banco, agencia, numeroConta);
		
		if(conta.getSaldo() < valorSaque) {
			throw new SaldoInsuficienteException();
		}
		conta.setSaldo(conta.getSaldo() -valorSaque);
		return conta;
	}
	

}
