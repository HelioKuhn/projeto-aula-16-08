package br.com.syonet.exerciciomaven;

public class Titular {

	private String nome;
	private String cpf;

	public Titular(String nome, String cpf) {
		super();
		this.nome = nome;
		this.cpf = cpf;
	}

	@Override
	public String toString() {
		return "Titular [nome=" + nome + ", cpf=" + cpf + "]";
	}

	public String getCpf() {
		return cpf;
	}

}
