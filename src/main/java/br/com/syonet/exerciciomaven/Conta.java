package br.com.syonet.exerciciomaven;

public class Conta {
	private Banco banco;
	private String agencia;
	private String numeroConta;
	private Double saldo;
	private EnumTipoConta tipoConta;
	private Titular titular;

	public Conta(Banco banco, String agencia, String numeroConta, Double saldo, EnumTipoConta tipoConta,
			Titular titular) {
		super();
		this.banco = banco;
		this.agencia = agencia;
		this.numeroConta = numeroConta;
		this.saldo = saldo;
		this.tipoConta = tipoConta;
		this.titular = titular;
	}

	@Override
	public String toString() {
		return "Conta [banco=" + banco + ", agencia=" + agencia + ", numeroConta=" + numeroConta + ", saldo=" + 
	String.format("%.2f", saldo)
				+ ", tipoConta=" + tipoConta + ", titular=" + titular + "]\n";
	}

	public Titular getTitular() {
		return titular;
	}

	public Banco getBanco() {
		return banco;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public String getAgencia() {
		return agencia;
	}

	public String getNumeroConta() {
		return numeroConta;
	}
	
	

}
